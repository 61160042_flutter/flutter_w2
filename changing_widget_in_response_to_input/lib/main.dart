import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

// control
class _CounterState extends State<StatefulWidget> {
  // _ mean private
  int _counter = 0;

  void _increament() {
    setState(() {
      _counter++;
    });
    // print = console.log
    print('Counter: $_counter');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: _increament, child: Text('Increament')),
        SizedBox(width: 16,),
        Text('Counter: $_counter'),
      ],
    );
  }

}
void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        body: Center(
          child: Counter(),
        )
      ),
    )
  );
}